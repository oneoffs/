#include <stdio.h> //printf()
#include <string.h> //memcpy
#include <limits.h> //INT_MAX
#include <stdlib.h> //rand(), srand()
#include <time.h> //time()

/* (*name_of_array)[] == how to dereference a pointer to an array*/
/* int *array[10] == an array of 10 int pointers*/
/* int (*array)[10] == dereferencing a pointer to an int which is stored at element 10 of the array "array".*/
/* int one = 1;*/ 
/* int two = 2;*/ 
/* int three = 3;*/
/* int four = 4;*/ 
/* int five = 5;*/ 
/* int* my_array[5] = {&one, &two, &three, &four, &five};*/ 
/* printf("%d", (*my_array)[3]); //prints the value 4 to the screen*/

#define ARRAY_SIZE 30

//malloc,calloc,realloc, dynamic memory has the scope of the pointer variable that points to it, but has the lifetime unlike any other, it is neither automatic/stack or part of the stack, but belongs to its own heap/dynamic/allocated memory space. Objects created here exist from the time they are created, until they are freed using the free() function. This can span multiple blocks and functions, or could happen 3 lines later if a free() function is used. However, the pointer that points to the block of memory is of whatever scope and lifetime it was created for and if it is lost/dies/goes out of scope this is a memory leak as their is no way to free the memory now and it will just keep taking up space that is no longer usable for future calls on heap memory (memory leak)
int main(void)
{
	srand(time(NULL));

	int static_array[ARRAY_SIZE];

	//fill static array with random numbers 0-9
	for (int i = 0; i < ARRAY_SIZE; ++i) {
			static_array[i] =  rand() % 9;
	}

	//print array
	for (int i = 0; i < ARRAY_SIZE; ++i) {
			printf("%d ", static_array[i]);
	}

	printf("\n\n");

	//dynamically create another array of twice the size and then copy over the contents
	// casts are not needed, but are often used to help other readers know whats going on, void pointer will be converted to whatever type the variable that is assigneed to the malloc is anyways even without cast
	/* int* heap_array = (int*) calloc(2, ARRAY_SIZE * sizeof(int)); //same as below but regardless of implementation it initializes all of the space to 0 */
	int* heap_array = (int*) malloc(ARRAY_SIZE * 2 * sizeof(int));
	memcpy(heap_array, static_array, ARRAY_SIZE * sizeof(int));

	//print out the new dynamically allocated array with the contents copied over
	for (int i = 0; i < ARRAY_SIZE * 2; ++i) {
			printf("%d ", heap_array[i]);
	}
		
	printf("\n\n");

	//I don't actually need it to be twice the size so truncate it to the original size + 3 
	//realloc does not change the size of th ptr you pass to it, but rather finds a block of memory of the size you want it to be and then copies over all the contents as if they were always stored in that size and frees the previous block of memory
	// if realloc is smaller than original than it truncates, and if it is larger the values of the positions in the array past the ones defined before are undefined (on my mac using clang it always initializes everything to 0 though (implementation defined))
	// often times you assign it to the same ptr you are changine the size of but technically you can just assign it to another ptr and then both the original and the new block will exist and both need to be freed in order to not have memory leaks, this is uncommon though but I guess is one way to copy memory from one to another, but is mroe cumbersome and confusing for others reading your code
	int* reallocated_array = realloc(heap_array, ARRAY_SIZE * sizeof(int) + (3 * sizeof(int)));

	//attempt to print it out as if I did not truncate it with realloc
	for (int i = 0; i < ARRAY_SIZE * 2; ++i) { //Implementation defined behavior going past array could all be initialized to 0, could segfault for going out of bounds, or could just return garbage
			printf("%d ", reallocated_array[i]);
	}

		printf("\n");

	free(reallocated_array); reallocated_array = NULL;
	/* free(reallocated_array); heap_array = NULL; */
	/* free(heap_array); heap_array = NULL; */
	/* free(heap_array); heap_array = NULL; //this line is just for educational purposes */  
	
	
	return 0;
}
