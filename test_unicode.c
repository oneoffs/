#include <stdio.h>
#include <wchar.h>
#include <stdlib.h>
#include <locale.h>

int main(void)
{
	setlocale(LC_CTYPE, "");
    	wchar_t white_king = 0x2654;
    	wchar_t black_king = 0x265A;
    	wprintf(L"%lc %lc: %c %c\n", white_king, black_king, 'K', 'k');
    	wprintf(L"WITHOUT VARIABLES: %lc %lc: %c %c\n", 0x2654, 0x265A, 'K', 'k');

	return 0;
}

