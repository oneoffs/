#include <stdio.h>


#include "my_atoi.h"


int main(int argc, char **argv)
{
	char input[3000];
	while (1) {
		printf("Enter number for my_atoi: ");
		scanf(" %s", input);
		printf("Using my very own my_atoi function I will return the string \"%s\" back in actual number form: %lf\n",input , my_atoi(input));
	}

	return 0;
}
