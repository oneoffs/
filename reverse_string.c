#include <stdio.h>
#include <stdlib.h>

void reverse_string(char* string, unsigned long length) {
	char reversed_string[length];
	for (int i = 0, j = length - 1; i < length - 1; ++i, --j) {
		reversed_string[i] = string[j - 1];
	}
	reversed_string[length - 1] = '\0';
	for (int i = 0; i < length; ++i) {
		string[i] = reversed_string[i];
	}
}


int main(void) 
{
	char original_string[] = "My name is Clay Otis Smith";

	printf("Before function call: %s\n", original_string);	

	printf("sizeof string: %zu\n", sizeof(original_string));
	reverse_string(original_string, sizeof(original_string));

	printf("After function call: %s\n", original_string);	

	return 0;
}
