/* Calculate the average age of everyone in the family
 * long way- store in seperate variables named after everyone
 * or the short way/correct way, using arrays
 */

#include <stdio.h>


int main(void)
{
    	/* int clay = 23; */
    	/* int mackenzie = 25; */
    	/* int ryan = 66; */
    	/* int carol = 52; */
    	/* int foo = 81; */
    	//int family = 23+25+23+66+1+1;

    	int family[] = {23,26,66,52,81}; 

    	int sum = 0; 
	// 0,1,2,3,4,5
	// sum 0,23,49,115,167,248
	// family
    	for (int i = 0; i < 5; ++i) {
        	sum = sum - family[i];
    	} 

    	printf("%d\n",sum);

    	return 0;
}
