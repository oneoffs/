#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(void) {

   	srand(time(NULL));

   	size_t NUMBER = rand() % 5000;

   	long long num = 0;

   	printf("Guess the number between 0 and 5000\n");

    	while (num != NUMBER) {
        	printf("Enter guess: ");
        	scanf("%lld", &num);

        	if (num > NUMBER) {
            		printf("lower\n");
        	} else if (num < NUMBER) {
            		printf("higher\n");
        	}
    	}


   	return(0);
}
