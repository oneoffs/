#include <stdio.h>
#include <string.h>
#include <stdlib.h>



int main(void)
{
	char* trimmed_input;

	{
		char large_input[900];
		int number_of_chars;

		printf("Enter text: ");
		fgets(large_input,899,stdin);
                number_of_chars = strnlen(large_input, 900);
		trimmed_input = malloc(sizeof(char) * number_of_chars);
		strncpy(trimmed_input, large_input, strnlen(large_input, 900));
	}

	printf("%s %lu\n", trimmed_input, strlen(trimmed_input));


	return 0;
}
