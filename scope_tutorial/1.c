#include <stdio.h>
#include "header.h"

int hello_num(int, int);
int a = 0;
int b = 0;

int main(void)
{
	int num = 1;

	{ 
		int num = 2;
	}

	auto double result;

//	double result2 = product_again(1,2); //results in error if uncommented because this function is specific to the file it was defined in
	int value = use_another_static_function(7, 3);

	result = product(1,2);

	int newNum = hello_num(1,2);

	printf("Newnum is: %d\na is: %d\tb is %d\n", newNum,a,b);

	printf("result is: %lf\n", result);

	printf("num is: %d\n", num);

	printf("use_another_static_function(7,3) returns: %d\n", value);

	return 0;
}


int hello_num(int a, int b) 
{
    	a++;
    	++b;    

    	return a;
}
