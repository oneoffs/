double product(double a, double b)
{
    	return a * b;
}

static double double_product(double a)
{
    	return a * 2;
}


double use_another_static_function(double a, double b)
{
	return double_product(product(a, b));
}
