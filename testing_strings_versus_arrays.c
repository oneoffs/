#include <stdio.h>

int main(void)
{
	char* foo = "Hello World!\n";
	char bar[] = "Hello World!\n";


	printf("The size of char* foo is: %zu\n", sizeof(foo));
	printf("The size of char bar[] is: %zu\n", sizeof(bar));
	return 0;
}
