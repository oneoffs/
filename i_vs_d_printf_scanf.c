#include <stdio.h>

int main()
{
	int a, b, c, d, e;     

	printf("scanf using %%i: ");    
	e = scanf("%i %i %i %i", &a, &b, &c, &d);    
	printf("%%i: %i, %%d: %d, %%i: %i, %%d: %d, e: %d\n\n",a,b,c,d,e);    

	printf("scanf using %%d: ");    
	e =scanf("%d %d %d %d", &a, &b, &c, &d);    
	printf("%%i: %i, %%d: %d, %%i: %i, %%d: %d, e: %d\n\n",a,b,c,d,e);    
    	return 0;
}
