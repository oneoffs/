#include <stdio.h>
void print_sum(int (*name)(int), int b) {
    	int a = name(b);
    	printf("The sum is: %d\n", a + b);
}
int subtract_one(int a) 
{
    	return a - 1;
}
int main(void)
{
    	print_sum(&subtract_one, 7);
    	return 0;
}
