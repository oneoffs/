#include <stdio.h>
#include <stdarg.h>

double add(int num_args, ...)
{
	va_list my_list;

	double i = 0, sum = 0;

	va_start(my_list, num_args);
	

	for (i = 0; i < num_args; ++i) {
		printf("%lf\n",sum += va_arg(my_list, int)); //everytime the va_arg (function?, macro?) is called it grabs the next argument in the list, but it is not smart enough to figure out if it has gone past the last argument
	}

	va_end(my_list);

	return sum;
}


int main(void)
{
	int a = 6, b = 3, c = 1, d = 4, e = 2;
	double sum;

	sum = add(5,a,b,c,d,e);

	printf("main function end: %lf\n",sum);

	printf("Printf using %%d, with the second paramater being 22 and the third being a+1 which would be 7. \n%d\n",22,a+1);
	printf("The WARNING message given to me by my clang apple compiler (BUT NOT AN ERROR):\nvariable_argument_functions2.c:32:116: warning: data argument not used by format string [-Wformat-extra-args]\n");
	//I included the above printf to show that at least with my compiler and most others that this code is valid and will compile.
	//While this code does throw warnings, there is no error and the program runs as if the 23 was not there. 
	//the reason for it not throwing any errors is because there is no implementation defined behavior for what were to happen if more arguments are passed
	//the operation expression still occurs, but the printf does nothing else with it

	return 0;
}

