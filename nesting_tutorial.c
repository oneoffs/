#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define WIDTH 100
#define LENGTH 100

struct max_min {
    	long long int max;
    	long long int min;
};


long long int return_max(long long int array[][LENGTH], int length, int width)
{
    	long long int max = array[0][0];
    	for (int i = 0; i < WIDTH; ++i) {
        	for (int j = 0; j < LENGTH; ++j) {
           		if (array[i][j] > max) {
               			max = array[i][j];
           		}
        	}
    	}
   	return max; 
}

struct max_min return_min(long long int array[][LENGTH], int length, int width)
{
    	struct max_min func_struct;
    	func_struct.min = array[0][0];
    	for (int i = 0; i < WIDTH; ++i) {
        	for (int j = 0; j < LENGTH; ++j) {
           		if (array[i][j] < func_struct.min) {
               			func_struct.min = array[i][j];
           		}
        	}
    	}
    	func_struct.max = return_max(array, length, width);
   	return func_struct; 
}

int main(void)
{
    	srand(time(NULL));
    	long long int two_d_array[WIDTH][LENGTH];

    	for (int i = 0; i < WIDTH; ++i) {
        	for (int j = 0; j < LENGTH; ++j) {
           		two_d_array[i][j] = rand() % 100000;
        	}
    	}
    	struct max_min newStruct = return_min(two_d_array, LENGTH, WIDTH);
    	printf("%lld\t%lld\n", newStruct.max, newStruct.min);

	//    printf("%lld, %lld, %lld\n", two_d_array[rand() % (WIDTH - 1)][0],two_d_array[rand() % (WIDTH - 1)][0],two_d_array[rand() % (WIDTH - 1)][0] );

    	return 0;
}
