#include <stdio.h>
#include <unistd.h>
#include <time.h>

#define NUM 1234567890
#define BUFSIZE	25

int main(void)
{
	int pid = fork();
	char buffer[BUFSIZE + 1];
	time_t time_taken;

	if (pid == 0) { //slightly slower result
		for (int i = 0; i < NUM; ++i) {
			printf("Child: %d\n", i);
		}	
		time_taken = time(&time_taken);
	} else { //slightly faster result
		for (int i = 0; i < NUM; ++i) {
			snprintf(buffer, BUFSIZE, "Parent: %d\n", i);
			write(1, buffer, BUFSIZE);
		}
		time_taken = time(&time_taken);
	}

	fprintf(stderr, "time taken: %zu", time_taken);
	return 0;
}
