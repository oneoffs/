#include <stdio.h>


int main(void)
{
    	enum days {Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday} test1;

    	enum days test2 = Tuesday;

    	test1 = Thursday;

    	enum days test3 = 65;

    	printf("%d\t%d\t%d\n", test1, test2, test3);

    	return 0;
}
